# awesomewm config
Here is the configuration to Awesome window manager. Awesome WM's documentation is confusing if you do not know what you are looking for. I craeted this configuration to help get you familiar to Awesome WM and the Lua programming language. I listed the programs I use so all the shortcuts to that program work out of the box. 

## Screenshot
![](/screenshot/awesomewm.png)

## Awesome WM config location:
```
~/.config/awesome/rc.lua
```

## autostart programs script location:
```
~/.config/aweosme/autorun.sh
```

## Remember to make autorun.sh executable
```
chmod +x ~/.config/aweosme/autorun.sh
```

## Add logout launcher from streetturtle's repostory called awesome-wm-widgets:
```
cd ~/.config/awesome/
git clone https://github.com/streetturtle/awesome-wm-widgets
git clone https://github.com/streetturtle/awesome-buttons
```

## Programs:
### Install these programs from your linux distribution package manager.
1. alacritty or terminal emulator of your choice 
2. dmenu
3. nemo
4. chromium/firefox
5. redshift
6. network-manager-applet
7. udiskie
8. polkit-gnome/policykit-1-gnome
9. geoclue2
10. pulseaudio
11. unclutter
12. discord
13. vol-notify
14. feh or nitrogen (optional) 

## Setting wallpaper
You can set wallpaper in the mytheme.lua file.

```
-- Set wallpaper 
theme.wallpaper = "path/to/wallpaper.png"
```

Example:
```
-- Set wallpaper 
theme.wallpaper = "~/Pictures/Wallpapers/peaceful-lake-1600×900.jpg"
```

## vol-notify
repo: https://gitlab.com/window-managers/vol-notify

## alacritty
On Ubuntu, alacritty must be compiled and installed from **cargo** or from **git**.<br>
repo: https://github.com/alacritty/alacritty

## Credit to the creators of awesomewm
Official Website: https://awesomewm.org/

Repository: https://github.com/awesomeWM/awesome

## Special Thanks: 
streetturtle: https://github.com/streetturtle 

streetturtle's repository (awesome-wm-widgets): https://github.com/streetturtle/awesome-wm-widgets 

## Wallpaper
### fluent wallpapers
https://github.com/vinceliuice/Fluent-gtk-theme

## My wallpaper collection
repo: https://github.com/sagedemage/wallpaper-collection
