#!/usr/bin/env bash

function run {
    if ! pgrep -f $1 ;
      then
          $@&
            fi
}

run nm-applet
run pasystray
run redshift-gtk
run udiskie
run unclutter &
run ulauncher --hide-window
# Ubuntu
run /usr/lib/policykit-1-gnome/polkit-gnome-authentication-agent-1
# Arch Linux
# run /usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1 
run /usr/lib/geoclue-2.0/demos/agent 
run xrdb ~/.Xresources 









